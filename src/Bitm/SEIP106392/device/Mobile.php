<?php

namespace App\Bitm\SEIP106392\device;

include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Mobile {
     public $id;
    public $model;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    public function __construct($model = false) {
        $this->model = $model;;
    }

    public function index() {
         $data = array();
        Connection::db_connect();
         $query = "SELECT * FROM `tbl_mobile`";
         $result =  mysql_query($query);
         while($row =  mysql_fetch_object($result)){
             $data[] = $row;
         }
         return $data;
    }

    public function create() {
        echo "  <h2 class='text-center'>I am create form</h2>";
    }

    public function store() {
        Connection::db_connect();
        $query = "INSERT INTO `tbl_mobile`( `mobile_model`) VALUES (' ". $this->model."')";
        $result =  mysql_query($query);
        if($result){
            Utility::message( "Your Mobile model is inserted !!");
        }else{
            Utility::message("Your Mobile model is not inserted !!");
        }
        Utility::redirect6();
    }

    public function edit() {
        echo " <h2 class='text-center'>I am editing form</h2>";
    }

    public function update($data = null) {
        Connection::db_connect();
//         echo '<pre>';
//        print_r($data);
//       exit();
        $id = $data['mobile_id'];
        $model = $data['mobile_model'];
        
        $query = "UPDATE `tbl_mobile` SET `mobile_id`='".$id."',`mobile_model`='".$model."' WHERE `mobile_id`=".$id;
//        Utility::debug($query);
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect6();
    }

    public function delete($mobile_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_mobile`WHERE `tbl_mobile`.`mobile_id`=".$mobile_id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Your data is successfully Deleted!!");
        }else{
            Utility::message("Unable to delete!!");
        }
        Utility::redirect6();
    }

     public function view($mobile_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_mobile` WHERE `tbl_mobile`.`mobile_id`=".$mobile_id;
         
        $result = mysql_query($query);
       
        $row = mysql_fetch_object($result);
        return $row;
    }
}
