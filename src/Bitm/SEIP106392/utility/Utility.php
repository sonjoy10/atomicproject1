<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utility
 *
 * @author sonjoy
 */
namespace App\Bitm\SEIP106392\utility;
session_start();
class Utility {
    static public function redirect($url="/atomicProject/views/SEIP106392/Birthday/index.php"){
        header("Location:".$url);
    }
    static public function redirect1($url="/atomicProject/views/SEIP106392/Book/index.php"){
        header("Location:".$url);
    }
    static public function redirect2($url="/atomicProject/views/SEIP106392/City/index.php"){
        header("Location:".$url);
    }
    static public function redirect3($url="/atomicProject/views/SEIP106392/Hobby/index.php"){
        header("Location:".$url);
    }
    static public function redirect4($url="/atomicProject/views/SEIP106392/Email_subscription/index.php"){
        header("Location:".$url);
    }
    static public function redirect5($url="/atomicProject/views/SEIP106392/organization/index.php"){
        header("Location:".$url);
    }
    static public function redirect6($url="/atomicProject/views/SEIP106392/Mobile/index.php"){
        header("Location:".$url);
    }
    static public function redirect7($url="/atomicProject/views/SEIP106392/Gender/index.php"){
        header("Location:".$url);
    }
    static public function redirect8($url="/atomicProject/views/SEIP106392/upload/index.php"){
        header("Location:".$url);
    }
    
    
    static public function message($msg = NULL){
        if(is_null($msg)){
            $_message = $_SESSION['message'];
            $_SESSION['message'] = null;
            return $_message;
        }else{
            $_SESSION['message'] = $msg;
        }
    }
    
    static public function debug($data){
        echo '<pre>';
        print_r($data);
        exit();
    }
}
