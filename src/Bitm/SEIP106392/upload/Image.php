<?php

namespace App\Bitm\SEIP106392\upload;

include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Image {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct() {
        Connection::db_connect();
    }

    public function index() {
        $data = array();

        $query = "SELECT * FROM `tbl_image` ";
        //Utility::debug($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $data[] = $row;
        }

        return $data;
    }

    public function view($image_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_image` WHERE `tbl_image`.`image_id`=" . $image_id;

        $result = mysql_query($query);

        $row = mysql_fetch_object($result);
        return $row;
    }

    public function update($file = null, $data = null) {
        //Utility::debug($data);
//        Utility::debug($file);
//        echo $name;

        if (isset($data['upload'])) {
            $id = $data['image_id'];
            $name = $data['name'];
            $image_name = $file['image_name']['name'];
            $image_type = $file['image_name']['type'];
            $image_size = $file['image_name']['size'];
            $image_temp_file = $file['image_name']['tmp_name'];
            $error = $file['image_name']['error'];

            $chk = move_uploaded_file($image_temp_file, "images/$image_name");

           if ($error > 0) {
            $name = $data['name'];
            $id = $data['image_id'];
            $query = "UPDATE `tbl_image` SET `name`='" . $name . "' WHERE `image_id`=" . $id;
//               Utility::debug($query);
            $result = mysql_query($query);
        } else if (file_exists("upload/$image_name")) {
            echo "$image_name.File already exists";
        } else if ($chk) {
            $image = $image_name;
            $query = "UPDATE `tbl_image` SET `image_id`='" . $id . "',`name`='" . $name . "',`image`='" . $image . "' WHERE `image_id`=" . $id;
//                 Utility::debug($query);  
            $result = mysql_query($query);
        }

        if ($result) {
            Utility::message("Your Image is Updated !!");
        } else {
            Utility::message("Your Image is not Updated !!");
        }

        Utility::redirect8();
    }

}

public function delete($image_id = null) {
    Connection::db_connect();
    $query = "DELETE FROM `tbl_image`WHERE `tbl_image`.`image_id`=" . $image_id;
    $result = mysql_query($query);
    if ($result) {
        Utility::message("Your data is successfully Deleted!!");
    } else {
        Utility::message("Unable to delete!!");
    }
    Utility::redirect8();
    }

    public

    function store($file = null, $data = null) {
//        Utility::debug($file);
        $name = $data['name'];
        if (isset($data['upload'])) {
            $image_name = $file['image_name']['name'];
            $image_type = $file['image_name']['type'];
            $image_size = $file['image_name']['size'];
            $image_temp_file = $file['image_name']['tmp_name'];
            $error = $file['image_name']['error'];

            $chk = move_uploaded_file($image_temp_file, "images/$image_name");

            if ($error > 0) {
                echo "Error";
            } else if (file_exists("upload/$image_name")) {
                echo "$image_name.File already exists";
            } else if ($chk) {


                $image = $image_name;
                $query = "INSERT INTO `tbl_image`(`name`, `image`) VALUES (' " . $name . "','" . $image . "')";
//                    
                $result = mysql_query($query);
//                    Utility::debug($result);
                if ($result) {
                    Utility::message("Your Image is inserted !!");
                } else {
                    Utility::message("Your Image is not inserted !!");
                }
            }

            Utility::redirect8();
        }
    }

}
