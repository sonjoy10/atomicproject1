<?php

namespace App\Bitm\SEIP106392\people;
use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Hobby {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($hobby_name = false) {
        ;
    }

    public function index() {
        $data = array();
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_hobby`";
        $result = mysql_query($query);
        while($row=  mysql_fetch_object($result)){
            $data[]=$row;
        }
        return $data;
    }
    public function view($hobby_id = null){
        Connection::db_connect();
       $query = "SELECT * FROM `tbl_hobby` WHERE `tbl_hobby`.`hobby_id`=".$hobby_id;
        $result = mysql_query($query);
        $row=  mysql_fetch_object($result);
        return $row;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store($hobby = null) {
        Connection::db_connect();
//        Utility::debug($hobby);
        $name= $hobby['name'];
        if(isset($hobby['save'])){
            if(isset($hobby['hobby'])){
                $multi =  implode(",", $hobby['hobby']) ;
            }else{
                $multi = "";
            }
        }
        
        $query = "INSERT INTO `tbl_hobby`(`name`, `hobby`) VALUES ('".$name."','".$multi."')";
//        Utility::debug($query);
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your hobby is successfully Inserted");
        }else{
            Utility::message("Unable to Insert");
        }
        Utility::redirect3();
    }

    public function edit() {
        echo " I am editing form";
    }

    public function update($hobby = null) {
         Connection::db_connect();
//        Utility::debug($hobby);
         $id= $hobby['hobby_id'];
        $name= $hobby['name'];
        if(isset($hobby['save'])){
            if(isset($hobby['hobby'])){
                $multi =  implode(",", $hobby['hobby']) ;
            }else{
                $multi = "";
            }
        }
        $query = "UPDATE `tbl_hobby` SET `hobby_id`='".$id."',`name`= '".$name."',`hobby`='".$multi."' WHERE `hobby_id`=".$id;
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your hobby is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect3();
    }
    

    public function delete($hobby_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_hobby` WHERE `tbl_hobby`.`hobby_id`=".$hobby_id;
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your hobby is successfully Deleted");
        }else{
            Utility::message("Unable to Delete");
        }
        Utility::redirect3();
    }

}
