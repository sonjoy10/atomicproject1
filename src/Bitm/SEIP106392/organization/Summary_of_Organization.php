<?php

namespace App\Bitm\SEIP106392\organization;

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Summary_of_Organization {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($summary = false) {
        $this->summary=$summary['summary'];
    }

    public function index() {
        $data = array();
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_summary` ";
        $result = mysql_query($query);
        while ($row = mysql_fetch_object($result)) {
            $data[] = $row;
        }
        return $data;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store() {
        Connection::db_connect();
        $query = "INSERT INTO `tbl_summary`( `summary`) VALUES (' ".$this->summary." ')";
        $result = mysql_query($query);
      // var_dump($result);
        if ($result) {
            $message = "Your Summary is inserted !!";
        } else {
            $message = "Your Summary  is not inserted !!";
        }
      Utility::redirect5();
    }

    public function edit() {
        echo " I am editing form";
    }

    public function update($data = null) {
        Connection::db_connect();
//         echo '<pre>';
//        print_r($data);
//       exit();
        $id = $data['summary_id'];
        $email = $data['summary'];
        
        $query = "UPDATE `tbl_summary` SET `summary_id`='".$id."',`summary`='".$email."' WHERE `summary_id`=".$id;
//        Utility::debug($query);
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect5();
    }

    public function delete($summary_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_summary`WHERE `tbl_email`.`summary_id`=".$summary_id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Your data is successfully Deleted!!");
        }else{
            Utility::message("Unable to delete!!");
        }
        Utility::redirect5();
    }
     public function view($summary_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_summary` WHERE `tbl_summary`.`summary_id`=".$summary_id;
//         Utility::debug($query);
        $result = mysql_query($query);
       
        $row = mysql_fetch_object($result);
        return $row;
    }

}
