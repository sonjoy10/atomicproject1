<?php

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP106392\upload\Image;

$img = new Image();
$data= $img->view($_REQUEST['id']);
?>
<!doctype Html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Profile Picture</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    
                    <h1 class="text-center">Profile Picture</h1>
                   
                    <form action="update.php" method="post"  enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label>Enter your name:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden"  name="image_id" class="form-control" value="<?php echo $data->image_id;?>" placeholder="Name"> 
                                            <input type="text"  name="name" class="form-control" value="<?php echo $data->name;?>" placeholder="Name"> 
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Please Select an image</label>
                                            <input type="file"  name="image_name" > 
                                            <img src="images/<?php echo $data->image;?>" alt="<?php echo $data->image;?>" style="width: 200px; height: 200px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary" name="upload"><strong><i class="fa fa-edit"></i> Edit</strong></button>                            
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
