<?php
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\education\Book_Title;

$book = new Book_Title();
$data = $book->view($_REQUEST['id']);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="text-center">Book Title</h1>
                    
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <form action="update.php" method="post">
                                <label for="exampleInputEmail1">Edit Your Book Title:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" value="<?php echo $data->book_id;?>" name="book_id">
                                            <input type="date" class="form-control"  value="<?php echo $data->book_title;?>" name="book_title" id="exampleInputEmail1" placeholder="Book_title">
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary"><strong><i class="fa fa-pencil"></i> Edit</strong></button>                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8  col-md-offset-3 m_top_30">
                                        <a href="index.php" class="btn btn-primary"><strong><i class="fa fa-list-alt"></i> List</strong></a>
                                        <a href="delete.php" class="btn btn-danger"><strong><i class="fa fa-trash"></i> Delete</strong></a>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                    
                </div>
            </div>

        </div>
    </body>
</html>
