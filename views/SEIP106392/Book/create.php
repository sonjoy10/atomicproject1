
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                        include_once '../../../vendor/autoload.php';
                        use \App\Bitm\SEIP106392\education\Book_Title;
                        $book= new Book_Title();                        
                    ?>
                    <h1 class="text-center">Book Title</h1>
                    <h4 class="text-center"><?php echo $book->create();?></h4>
                    <form action="store.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Book:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="text" class="form-control" name="book_title" id="exampleInputEmail1" placeholder="Book Title">
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary"><strong><i class="fa fa-floppy-o"></i> Save</strong></button>                            
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
        <script type="text/javascript" src="stylesheet" href="../../../assets/js/jquery.js"></script>
    </body>
</html>
