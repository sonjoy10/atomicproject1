
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                        include_once("../../../vendor/autoload.php");
                        use App\Bitm\SEIP106392\media\Email_subscription;
                        use \App\Bitm\SEIP106392\utility\Utility;
                        $obj=new Email_subscription();   
                        $email=$obj->index();
                        $u = Utility::message();
                    ?>
                    <h1 class="text-center">Email Subscription</h1>
                    <?php
                    if (isset($u)) {
                        ?>
                        <div class="alert alert-success" role="alert"><h4><?php echo $u; ?></h4></div>
                        <?php
                    }
                    ?>
                    <a href="create.php" class="btn btn-primary add">Add</a>
                    <a href="#" class="btn btn-primary add">Download As Pdf</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sl=1;
                                foreach ($email as $value) {
                            ?>
                            <tr>
                                <td><?php echo $sl;?></td>
                                <td><a href="view.php?id=<?php echo $value->email_id;?>"><?php echo $value->email_address;?></a></td>
                                <td>
                                        <a href="view.php?id=<?php echo $value->email_id;?>"><button type="button" class="btn btn-success"><i class="fa fa-search"></i></button></a>
                                        <a href="edit.php?id=<?php echo $value->email_id; ?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
                                        <a href="delete.php?id=<?php echo $value->email_id; ?>" onclick="return check();"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                                        <a href="delete.php"><button type="button" class="btn btn-warning">Trash/Recover</button></a>
                                        <a href="delete.php"><button type="button" class="btn btn-default">Email to Friend</button></a>
                                    </td>
                            </tr>
                            <?php
                                $sl++;
                                 }
                            ?>

                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        
        <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
        <script>
            function check(){
                var chk =confirm("Are you sure to Delete?");
                if(chk){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </body>
</html>
