<?php
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\person\Gender;

$gender = new Gender();
$data = $gender->view($_REQUEST['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gender</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">

                    <h1 class="text-center">Gender</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Name:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" class="form-control" name="gender_id" id="exampleInputEmail1" value="<?php echo $data->gender_id; ?>" >
                                            <input type="text" class="form-control" name="name" id="exampleInputEmail1" value="<?php echo $data->name; ?>" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="radio">
                                                    <label for="exampleInputEmail1">Select Your Gender:</label>
                                                    <?php
                                                    if ($data->gender == 1) {
                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gender" id="inlineRadio1" value="1" checked> Male
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gender" id="inlineRadio2" value="2"> Female
                                                        </label>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gender" id="inlineRadio1" value="1"> Male
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gender" id="inlineRadio2" value="2" checked> Female
                                                        </label>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <button type="submit" class="btn btn-primary"><strong><i class="fa fa-floppy-o"></i> Edit</strong></button>                            
                                            </div>
                                        </div>
                                    </div>
                                    <a href="index.php" class="btn btn-primary">Go to list</a>  
                                </div>
                            </div>


                        </div>
                    </form>
                </div>

            </div>
    </body>
</html>
