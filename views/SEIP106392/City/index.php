
<html>
    <head>
        <meta charset="UTF-8">
        <title>City</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                    include_once '../../../vendor/autoload.php';

                    use App\Bitm\SEIP106392\place\City;
                    use \App\Bitm\SEIP106392\utility\Utility;

$city = new City();
                    $city_name = $city->index();
                    $u = Utility::message();
                    ?>
                    <h1 class="text-center">City List</h1>
                    <?php
                    if (isset($u)) {
                        ?>
                        <div class="alert alert-success" role="alert"><h4><?php echo $u; ?></h4></div>
                        <?php
                    }
                    ?>
                    <a href="create.php" class="btn btn-primary add">Add</a>
                    <a href="#" class="btn btn-primary add">Download As Pdf</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>City</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sl = 1;
                            foreach ($city_name as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><a href="view.php?id=<?php echo $value->city_id;?>"><?php echo $value->city_name; ?></a></td>
                                     <td>
                                        <a href="view.php?id=<?php echo $value->city_id;?>"><button type="button" class="btn btn-success"><i class="fa fa-search"></i></button></a>
                                        <a href="edit.php?id=<?php echo $value->city_id; ?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
                                        <a href="delete.php?id=<?php echo $value->city_id; ?>" onclick="return check();"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                                        <a href="delete.php"><button type="button" class="btn btn-warning">Trash/Recover</button></a>
                                        <a href="delete.php"><button type="button" class="btn btn-default">Email to Friend</button></a>
                                    </td>
                                </tr>
                                <?php
                                $sl++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        
        <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
        <script>
            function check(){
                var chk =confirm("Are you sure to Delete?");
                if(chk){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </body>
</html>
