<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bitm Web App Deb PHP</title>
        <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <header class="  text-center" style="background: #2ecc71; color: #FFFFFF;">
            <div class="container">
                <h1>BITM -Web App Dev - PHP</h1>
                <h2 >Atomic Project</h2>
                <h3 >Name: Sonjoy Biswas</h3>
                <h3 >SEID : 106392</h3>
                <h4>Batch: 11</h4>
            </div>
        </header>


        <section  class=""style="margin-top: 50px;min-height:650px;">
            <h3 class="text-center" style="background: #e67e22; color: #FFFFFF; padding: 10px 0;margin-bottom: 20px">Projects</h3>
            <div class="row container">
                <div class="col-md-6 col-md-offset-4">
                    <table class="table table-condensed m_top_30">
                        <tr>
                            <th>SL</th>
                            <th>Project Name</th>
                        </tr>
                        <tr>
                            <td>01</td>
                            <td><a href="views/SEIP106392/Book">Book Title</a></td>
                        </tr>
                        <tr>
                            <td>02</td>
                            <td><a href="views/SEIP106392/Mobile">Mobile Model</a></td>
                        </tr>
                        <tr>
                            <td>03</td>
                            <td><a href="views/SEIP106392/Hobby">Hobby</a></td>
                        </tr>
                        <tr>
                            <td>04</td>
                            <td><a href="views/SEIP106392/city">City</a></td>
                        </tr>
                        <tr>
                            <td>05</td>
                            <td><a href="views/SEIP106392/Email_subscription">Email Subscription</a></td>
                        </tr>
                        <tr>
                            <td>06</td>
                            <td><a href="views/SEIP106392/organization">Summary of Organization</a></td>
                        </tr>
                        <tr>
                            <td>07</td>
                            <td><a href="views/SEIP106392/Birthday">Birthday</a></td>
                        </tr>
						<tr>
                            <td>08</td>
                            <td><a href="views/SEIP106392/upload">Image</a></td>
                        </tr>
                        <tr>
                            <td>09</td>
                            <td><a href="views/SEIP106392/Gender">Gender</a></td>
                        </tr>
                    </table>

                </div>
            </div>

        </section>
        <footer class=" text-center m_top_30 footer" style="background: #34495e; color: #FFFFFF;">
            <div class="container">
                <h6>Copy right &copy Atomic project by Sonjoy Biswas </h6>
            </div>
        </footer>

    </body>
</html>
